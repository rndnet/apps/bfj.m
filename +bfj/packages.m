function p = packages()
    z = fullfile('in', '.data.zip');
    if exist(z, 'file') == 2
        unzip(z, 'in')
    end
    files = dir(fullfile('in', '*'));
    p = {};
    for k = 1:numel(files)
        f = files(k);
        if strcmp(f.name, '.') || strcmp(f.name, '..')
            continue
        end
        f = fullfile('in', f.name);
        if exist(f, 'dir') == 7
            p{end+1} = struct('path', f, 'label', bfj.package_label(f));
        end
    end
end
