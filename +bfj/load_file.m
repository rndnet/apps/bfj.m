function p = load_file(fname)
    try
        p = load(fname, '-mat');
    catch
        p = load_h5(fname, h5info(fname));
    end
end

function p = load_h5(fname, info)
    p = struct();

    for i = 1:length(info.Datasets)
        n = info.Datasets(i).Name;
        if (length(n) > 1) && strcmp(n(1:2), '__')
            continue
        end
        p.(n) = h5read(fname, [info.Name, '/', n]);
    end

    for i = 1:length(info.Groups)
        n = info.Groups(i).Name;
        t = split(n, '/');
        n = t{end};
        p.(n) = load_h5(fname, info.Groups(i));
    end
end
