function run_script(script)

try
    run(script)
catch e
    getReport(e)
    exit(1)
end

exit(0)
