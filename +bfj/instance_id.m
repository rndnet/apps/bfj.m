function i = instance_id()
    [~,stem,~] = fileparts(pwd());
    i = str2num(stem);
end
