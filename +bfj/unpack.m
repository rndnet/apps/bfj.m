function unpack(s)
    fields = fieldnames(s);
    for k = 1:numel(fields)
        f = fields{k};
        assignin('caller', f, s.(f));
    end
end
