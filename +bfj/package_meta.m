function p = package_meta(path)
    p = struct();
    
    files = dir(fullfile(path, 'meta', '*'));
    for k = 1:numel(files)
        f = files(k);

        if strcmp(f.name, '.') || strcmp(f.name, '..')
            continue
        end
        
        v = f.name;
        f = fullfile(path, 'meta', f.name);

        if exist(f, 'file') ~= 2
            continue
        end

        p.(v) = json_decode(fileread(f));
    end
end
