function label = package_label(path)
    label_file = fullfile(path, 'label');
    if exist(label_file, 'file') == 2
        label = fileread(label_file);
    else
        label = '';
    end
end
