function p = load_package(path, load_meta)
    links = fullfile(path, '.files');
    if exist(links, 'file') == 2
        files = json_decode(fileread(links));
        for i = 1:numel(files)
            system(['wget -q "' files(i).link '" -O "' fullfile(path, files(i).name) '"']);
        end
    end

    if nargin < 2
        load_meta=true;
    end

    p = struct();

    if exist(path, 'dir') ~= 7
        return
    end

    files = dir(fullfile(path, '*'));
    for k = 1:numel(files)
        f = files(k);
        if strcmp(f.name, '.') || strcmp(f.name, '..') || strcmp(f.name, 'label') || strcmp(f.name, '.files')
            continue
        end
        f = fullfile(path, f.name);
        if exist(f, 'file') ~= 2
            continue
        end

        [~,var,~] = fileparts(f);

        try
            t = bfj.load_file(f);
            fields = fieldnames(t);
            if numel(fields) == 1
                p.(var) = t.(fields{1});
            else
                p.(var) = t;
            end
        catch e
            fprintf('Skipped "%s". Reason:\n', f);
            getReport(e)
        end
    end

    if load_meta
        p = bfj.merge_structs(p, bfj.package_meta(path));
    end
end
