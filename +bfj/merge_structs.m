function s = merge_structs(varargin)
    s = struct();
    for k = 1:numel(varargin)
        t = varargin{k};
        fields = fieldnames(t);
        for i = 1:numel(fields)
            f = fields{i};
            s.(f) = t.(f);
        end
    end
end
