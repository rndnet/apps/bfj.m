function p = read_params(fname)
    if nargin < 1
        fname = '.prm';
    end
    p = struct();

    if exist(fname, 'file') == 2
        v = json_decode(fileread(fname));
        for i = 1:numel(v)
            p.(v(i).key) = json_decode(v(i).val);
        end
    end
end
