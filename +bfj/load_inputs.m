function p = load_inputs()
    p = bfj.read_params();

    pkg = bfj.packages();
    for k = 1:numel(pkg)
        p = bfj.merge_structs(p, bfj.load_package(pkg{k}.path));
    end
end
