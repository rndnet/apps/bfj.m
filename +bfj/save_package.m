function path = save_package(varargin)
    global package_index

    label = '';
    data  = struct();
    meta  = struct();
    image = struct();
    
    bfj.unpack(struct(varargin{:}));

    if isempty(package_index)
        package_index = 1;
    else
        package_index = package_index + 1;
    end

    path = fullfile('out', num2str(package_index));
    mkdir(path);

    if ~isempty(label)
        f = fopen(fullfile(path, 'label'), 'w');
        fprintf(f, '%s', label);
        fclose(f);
    end

    fields = fieldnames(data);
    for k = 1:numel(fields)
        n = fields{k};
        save(fullfile(path, [n '.h5']), '-v7.3', '-struct', 'data', n);
    end

    fields = fieldnames(meta);
    if ~isempty(fields)
        mkdir(fullfile(path, 'meta'));
    end
    for k = 1:numel(fields)
        n = fields{k};
        f = fopen(fullfile(path, 'meta', n), 'w');
        fprintf(f, '%s', json_encode(meta.(n)));
        fclose(f);
    end

    fields = fieldnames(image);
    if ~isempty(fields)
        mkdir(fullfile(path, 'image'));
    end
    for k = 1:numel(fields)
        n = fields{k};
        print(image.(n), fullfile(path, 'image', [n '.png']), '-dpng');
    end
end
