# BFJ.m

```sh
git clone https://gitlab.com/rndnet/apps/bfj.m.git
cd bfj.m
export MATLABPATH=${MATLABPATH}:${PWD}
export PATH=${PATH}:${PWD}
```

The following runs `my_script.m` with matlab and returns the correct exit code
(0 for success, 1 for failure):
```sh
bfj_m_run my_script.m
```

## Dependencies
* wget
* https://github.com/leastrobino/matlab-json